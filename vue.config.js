// vue.config.js
module.exports = {
    // https://cli.vuejs.org/zh/config/#publicpath
    publicPath: process.env.NODE_ENV === 'production' ? '/s/' : '/', //部署到码云的 /xxx 项目中

    // 选项...
    productionSourceMap: false
}
